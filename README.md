# ToDo List for Android

## Introduction

> A simple ToDo List in Java for Android OS

## Detailed Function

You can add tasks and delete them.

## Screenshot

| <a href="https://gitlab.com/haidepzai/todo-list-android" target="_blank">**ToDo List Android**</a>


| [![Hai](https://i.ibb.co/mSm1DLD/ToDo.jpg)](https://gitlab.com/haidepzai/todo-list-android)