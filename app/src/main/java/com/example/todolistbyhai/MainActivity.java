package com.example.todolistbyhai;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private EditText itemET;
    private ImageButton btn;
    private ListView itemsList;
    private ImageButton buttons;

    private ArrayList<String> items; //Contains the To Do Elements representing in a ArrayList
    private ArrayAdapter<String> adapter; //For ListView


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        itemET = findViewById(R.id.item_edit_text);
        btn = findViewById(R.id.add_btn);
        itemsList = findViewById(R.id.items_list);

        items = FileHelper.readData(this);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_activated_1, items);
        itemsList.setAdapter(adapter);

        btn.setOnClickListener(this);
        itemsList.setOnItemClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.add_btn:
                String itemEntered = itemET.getText().toString();
                adapter.add(itemEntered); //Add the Element to the ArrayAdapter
                itemET.setText("");
                FileHelper.writeData(items, this);
                Toast.makeText(this, "Item Added", Toast.LENGTH_SHORT).show();
                break;
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        //Background Color for ListView
        for (int i = 0; i < itemsList.getChildCount(); i++) {
            if(position == i ){
                itemsList.getChildAt(i).setBackgroundColor(Color.rgb(191, 255, 212));
            }else{
                itemsList.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
            }
        }
        //Initialize Delete Button and make Visible OnClick
        buttons = findViewById(R.id.deleteBTN);
        buttons.setVisibility(View.VISIBLE);
        buttons.setOnClickListener(new View.OnClickListener() {
            @Override //onClick Delete Button = Remove selected Item
            public void onClick(View v) {
                items.remove(position);
                adapter.notifyDataSetChanged();
                buttons.setVisibility(View.INVISIBLE);
                Toast.makeText(MainActivity.this, "Deleted", Toast.LENGTH_SHORT).show();
            }
        });
        FileHelper.writeData(items, this);

    }
}